%%
% Given a linear program of the form:
%   min c^T x
%   s.t.Mx = r
%   x >= 0
%
%   Return A, b such that a solution to Ay = b can be used to find the
%   optimal solution of the original linear problem
%%

function [M, w] = convertLPtoNNLS(A, b, c)
    [numConstraintsOriginal, numVariablesOriginal] = size(A);
    M = zeros(numConstraintsOriginal + numVariablesOriginal + 1, 2 * numVariablesOriginal + 2 * numConstraintsOriginal);
    
    M(1:numConstraintsOriginal, 1:numVariablesOriginal) = A;
    M(numConstraintsOriginal + 1, :) = [-c; b; -b; zeros(numVariablesOriginal, 1)].';
    M(numConstraintsOriginal + 2:end, :) = [zeros(numVariablesOriginal)  A.'  -A.'  eye(numVariablesOriginal)];
    
    w = [b; 0; c];
end
