% Test solving a linear program using gradient descent method
% Given a linear program of the form min c^T x s.t. A x = b, x >= 0
% use first order methods to find the optimal x

% create the parameters for the problem
numRows = 5;
numCols = 10;

A = reshape(randn(numRows * numCols ,1), numRows, numCols);
b = reshape(randn(numRows, 1), numRows, 1);
c = abs(reshape(randn(numCols, 1), numCols, 1));

lb = zeros(numCols, 1);
ub = [];

% Use Matlab to solve linear program
[xMatlab, fopt] = linprog(c,[],[],A,b,lb,ub);

% Convert Linear program to solve First Order Method Problem
[M, w] = convertLPtoNNLS(A, b, c);
[xopt,OVopt,niter,epsilon] = PartA_i(M, w);

% Recover the LP solution from the First Order Method Problem
xFOM = xopt(1:numCols);
fOptFOM = c.' * xFOM