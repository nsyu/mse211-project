function [A, b, c] = convert_data(truncateSize)

filename = 'assign100.dat';
M = csvread(filename);

M = M(1:truncateSize, 1:truncateSize);
[numWorkers, numJobs] = size(M);

numConstraints = numWorkers + numJobs;
A = zeros(numConstraints, numWorkers * numJobs);

% each job only has exactly one worker assigned
for jobIndex = 1:numJobs
    A(jobIndex, 1 + (jobIndex  - 1) * numWorkers : jobIndex * numWorkers) = ones(1, numWorkers);
end

% each worker is only assigned 1 job
for workerIndex = 1:numWorkers
    offset = numJobs;
    x = zeros(1, numJobs);
    x(workerIndex) = 1;
    x_full = repmat(x, 1, numWorkers);
    A(workerIndex + offset, :) = x_full;
end

% variables for min cx s.t. Ax = b

b = ones(numConstraints, 1);
c = reshape(M,[],1);

end

