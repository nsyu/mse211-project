function [xopt,OVopt,niter,epsilon] = PartA_i(A, b)
%b is a column vector

tic

% initial starting guess for solution, x
n= size(A,2);
x0= abs(rand(n,1));

% maximum allowed number of iterations
maxiter = 600;

% step size
alpha = 0.0000004;

% initialize iteration number
niter = 0; 

% calculate Matlab solution, xMatlab for comparison
xMatlab= lsqnonneg(A,b);
errors_k= zeros(1, maxiter);
errors_kM= zeros(1, maxiter);

% perform the gradient descent algorithm
x= x0;
xM = x0; 
mu = 0.99;
vM = zeros(size(x));

% Precompute some values for gradient
TwoTimesATransposeA = 2 * transpose(A) * A;
TwoTimesATransposeb = 2 * transpose(A) * b;

%
    function g = computeGradient(x)
        g = TwoTimesATransposeA * x - TwoTimesATransposeb;
    end

    function x = vanillaUpdate(x)
        x = x - alpha * computeGradient(x);
    end

    function [x, v] = momentumUpdate(x, v)
        v = mu * v - alpha * computeGradient(x);
        x = x + v;
    end

while (niter <= maxiter)
    niter
    % new step
    % calculate the gradient of ||Ax - b|| ^ 2 with respect to x
    x_prev = x;
    xM_prev = xM;
    
    x = vanillaUpdate(x);    
    [xM, vM] = momentumUpdate(xM, vM);
    
    % maintain x >= 0
    x = max(x, 0);
    xM = max(xM, 0);
    % update variables
    niter = niter + 1;
    epsilon = norm(x - x_prev); 
    errors_k(niter)= (norm(A*x-b))^2;
    errors_kM(niter)= (norm(A*xM-b))^2;
end

% final answers
xopt = x;                   % solution
OVopt = (norm(A*xopt-b))^2; % objective function value
niter = niter - 1;          % number of iterations
plot(1:niter, (norm(A*xMatlab-b))^2*ones(1,niter), '*', 1:niter, errors_k(1:niter),'o', 1:niter, errors_kM(1:niter),'+');
xlabel('number of iterations')
ylabel('Error, ||Ax-b||^2')
legend('x solved with Matlab function lsqnonneg','x solved with gradient descent', 'x solved with momentum gradient descent')
set(gca,'FontSize',20)

toc

end
