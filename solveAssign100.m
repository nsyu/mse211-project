% This code tests our First Order Method code against Matlabs non negative
% least squares code

% test on random matrix 
numRows = 10000;
numCols = 1000;

% Generate Parameters
A = reshape(randn(numRows * numCols ,1), numRows, numCols);
b = reshape(randn(numRows, 1), numRows, 1);

% Solve NNLS using built in matlab function lsnonneg
[least_squares_solution,resnorm,residual]  = lsqnonneg(A, b);

% NNLS using our first order method code
[xopt,OVopt,niter,epsilon] = PartA_i(A, b);

% How do our residuals differ?
A * xopt - b + residual; % our residual + matlab residual

% all of these should be equal
norm(residual) ^ 2; % sum of squares of residuals
resnorm % sum of squares of residuals
OVopt % sum of squares of residuals