% Read the data in from assign100.txt
truncateSize = 30;
[A, b, c] = convert_data(truncateSize);

% convert the LP to a NNLS Problem
[M, w] = convertLPtoNNLS(A, b, c);
M = sparse(M);

% Solve the NNLS using our FOM code
[xopt,OVopt,niter,epsilon] = PartA_i(M, w);

% Recover the optimal solution using built in NNLS
[least_squares_solution,resnorm,residual] = lsqnonneg(M, w);
