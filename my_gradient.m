function my_gradient(A, x, b)
    % Return the gradient of |Ax - b| ^ 2 with respect to x
    
    2 * transpose(A) * A * x - 2 * transpose(A) *  b
    
end
